package com.example.longta.androidworkshopproject.Util;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by longta on 6/7/17.
 */

public class LoadImageUtil {
    public static void loadImage(String imageURL, ImageView imageview, Context context){
        String imageURLCompleted = "http:" + imageURL;
        Picasso.with(context).load(imageURLCompleted).into(imageview);
    }
}
