package com.example.longta.androidworkshopproject.Util;

import android.content.Context;

import com.example.longta.androidworkshopproject.Models.Country;

import java.util.ArrayList;

/**
 * Created by longta on 6/9/17.
 */

public class StorageUtil {
    public static final String BOOKMARK_COUNTRIES = "BookmarkCountries";

    public static void bookmarkCountry(Country country, Context context) {
        TinyDB tinydb = new TinyDB(context);

        ArrayList<String> mListBookmarkCountry = tinydb.getListString(BOOKMARK_COUNTRIES);

        if (mListBookmarkCountry.contains(country.getCityName())){
            mListBookmarkCountry.remove(country.getCityName());
        }
        else{
            mListBookmarkCountry.add(country.getCityName());
        }

        //save
        tinydb.putListString(BOOKMARK_COUNTRIES, mListBookmarkCountry);
    }

    public static ArrayList<String> getListBookmarkedCountries(Context context){
        TinyDB tinydb = new TinyDB(context);
        ArrayList<String> mListBookmarkCountry = tinydb.getListString(BOOKMARK_COUNTRIES);
        return  mListBookmarkCountry;
    }

    public static boolean isCountryBookmarked(Country country, Context context){
        TinyDB tinydb = new TinyDB(context);

        ArrayList<String> mListBookmarkCountry = tinydb.getListString(BOOKMARK_COUNTRIES);
        if (mListBookmarkCountry.contains(country.getCityName())){
            return true;
        }
        return false;
    }
}
