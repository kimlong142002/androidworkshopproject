package com.example.longta.androidworkshopproject.ViewHolder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.longta.androidworkshopproject.JsonModels.Forecastday;
import com.example.longta.androidworkshopproject.R;
import com.example.longta.androidworkshopproject.Util.LoadImageUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by longta on 6/6/17.
 */

public class ForecastNextDaysViewHolder extends RecyclerView.ViewHolder {
    // Views
    private TextView mTextDay, mTextMinTemp, mTextMaxTemp;
    private ImageView mImageViewCondition;

    public ForecastNextDaysViewHolder(View itemView) {
        super(itemView);

        mTextDay = (TextView) itemView.findViewById(R.id.text_day);
        mImageViewCondition = (ImageView) itemView.findViewById(R.id.imageView_day_condition);
        mTextMinTemp = (TextView) itemView.findViewById(R.id.text_min_day_temp);
        mTextMaxTemp = (TextView) itemView.findViewById(R.id.text_max_day_temp);
    }

    public void setData(Forecastday forecastday, Context context) throws ParseException {

        SimpleDateFormat dateFormatOrigin = new SimpleDateFormat("yyyy-MM-dd");
        Date date = dateFormatOrigin.parse(forecastday.getDate());
        SimpleDateFormat dateFormatNew = new SimpleDateFormat("EEE");
        String formatedDay = dateFormatNew.format(date);
        mTextDay.setText(formatedDay);

        LoadImageUtil.loadImage(forecastday.getDay().getCondition().getIcon(), mImageViewCondition, context);

        String minTemperatureString = String.format("%.0f", forecastday.getDay().getMintempC());
        mTextMinTemp.setText(minTemperatureString);

        String maxTemperatureString = String.format("%.0f", forecastday.getDay().getMaxtempC());
        mTextMinTemp.setText(minTemperatureString);
    }
}
