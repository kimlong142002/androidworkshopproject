package com.example.longta.androidworkshopproject.RESTFactory;

import com.squareup.otto.Bus;

/**
 * Created by longta on 5/29/17.
 */

public class BusProvider {
    private static final Bus BUS = new Bus();

    public static Bus getInstance(){
        return BUS;
    }

    public BusProvider(){}
}
