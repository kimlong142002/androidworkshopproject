package com.example.longta.androidworkshopproject.JsonModels;

/**
 * Created by longta on 5/30/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ForecastResponse {

    @SerializedName("location")
    @Expose
    private CurrentLocation currentLocation;
    @SerializedName("current")
    @Expose
    private Current current;
    @SerializedName("forecast")
    @Expose
    private Forecast forecast;

    public CurrentLocation getCurrentLocation() {
        return currentLocation;
    }

    public void setLocation(CurrentLocation currentLocation) {
        this.currentLocation = currentLocation;
    }

    public Current getCurrent() {
        return current;
    }

    public void setCurrent(Current current) {
        this.current = current;
    }

    public Forecast getForecast() {
        return forecast;
    }

    public void setForecast(Forecast forecast) {
        this.forecast = forecast;
    }

}
