package com.example.longta.androidworkshopproject.Models;

import com.example.longta.androidworkshopproject.JsonModels.SearchResult;

import java.util.List;

public class SearchEvent {
    private List<SearchResult> searchResults;
    public SearchEvent(List<SearchResult> serverResponses) {
        this.searchResults = serverResponses;
    }

    public List<SearchResult> getSearchResults() {
        return searchResults;
    }

    public void setSearchResults(List<SearchResult> searchResults){
        this.searchResults = searchResults;
    }
}
