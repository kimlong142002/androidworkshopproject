package com.example.longta.androidworkshopproject.Models;

import com.example.longta.androidworkshopproject.JsonModels.ForecastResponse;

/**
 * Created by longta on 5/29/17.
 */

public class ServerEvent {
    private ForecastResponse forecastResponse;

    public ServerEvent(ForecastResponse serverResponse) {
        this.forecastResponse = serverResponse;
    }

    public ForecastResponse getForecastResponse() {
        return forecastResponse;
    }
    public void setForecastResponse(ForecastResponse forecastResponse) {
        this.forecastResponse = forecastResponse;
    }

}

