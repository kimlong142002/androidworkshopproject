package com.example.longta.androidworkshopproject.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.longta.androidworkshopproject.JsonModels.Hour;
import com.example.longta.androidworkshopproject.R;
import com.example.longta.androidworkshopproject.ViewHolder.ForecastHourViewHolder;

import java.text.ParseException;
import java.util.List;

/**
 * Created by longta on 6/6/17.
 */

public class ForecastHourAdapter extends RecyclerView.Adapter {
    private LayoutInflater mLayoutInflater;
    public List<Hour>mHourList;
    public Context mContext;

    public ForecastHourAdapter(Context context, List<Hour>hourList){
        mLayoutInflater = LayoutInflater.from(context);
        mHourList = hourList;
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ForecastHourViewHolder(mLayoutInflater.inflate(R.layout.recycler_view_forecast_hour, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ForecastHourViewHolder forecastHourViewHolder = (ForecastHourViewHolder)holder;
        Hour hour = mHourList.get(position);
        try {
            forecastHourViewHolder.setData(hour, mContext);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mHourList.size();
    }
}
