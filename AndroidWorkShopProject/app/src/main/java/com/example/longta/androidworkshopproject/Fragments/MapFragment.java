package com.example.longta.androidworkshopproject.Fragments;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.longta.androidworkshopproject.JsonModels.Current;
import com.example.longta.androidworkshopproject.JsonModels.CurrentLocation;
import com.example.longta.androidworkshopproject.Models.Country;
import com.example.longta.androidworkshopproject.Models.ErrorEvent;
import com.example.longta.androidworkshopproject.Models.ServerEvent;
import com.example.longta.androidworkshopproject.R;
import com.example.longta.androidworkshopproject.RESTFactory.BusProvider;
import com.example.longta.androidworkshopproject.RESTFactory.Communicator;
import com.example.longta.androidworkshopproject.Util.DialogUtil;
import com.example.longta.androidworkshopproject.Util.GPSTracker;
import com.example.longta.androidworkshopproject.Util.LoadImageUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.otto.Subscribe;

/**
 * Created by longta on 5/28/17.
 */

public class MapFragment extends Fragment implements OnMapReadyCallback{

    GoogleMap ggMap;
    private TextView textViewCityName, textViewCondition, textViewTemperature, textViewCelsius;
    private ImageView imageViewCondition;
    private Button buttonNext;
    private Communicator communicator;
    private GPSTracker gpsTracker;

    public static MapFragment newInstance() {
        return new MapFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);

        //Init textviews
        LinearLayout linearLayout = (LinearLayout) view;
        textViewCityName = (TextView) linearLayout.findViewById(R.id.text_city_name);
        textViewCondition = (TextView) linearLayout.findViewById(R.id.text_condition);
        textViewTemperature = (TextView) linearLayout.findViewById(R.id.text_temperature);
        textViewCelsius = (TextView) linearLayout.findViewById(R.id.text_celsius);
        imageViewCondition = (ImageView) linearLayout.findViewById(R.id.imageView_condition);
        buttonNext = (Button)linearLayout.findViewById(R.id.button_next);

        textViewCityName.setTextColor(getResources().getColor(R.color.grayColor));
        textViewCondition.setTextColor(getResources().getColor(R.color.darkGrayColor));
        textViewTemperature.setTextColor(getResources().getColor(R.color.darkGrayColor));
        textViewCelsius.setTextColor(getResources().getColor(R.color.darkGrayColor));

        //Init communicator
        communicator = new Communicator();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
        if (ggMap != null){
            onMapReady(ggMap);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
        ((MapFragmentListener)getActivity()).onDisableShareButtonEvent();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (!isAdded()){
            return;
        }
        buttonNext.setVisibility(View.GONE);
        ggMap = googleMap;

        // check if GPS enabled
        gpsTracker = new GPSTracker(getActivity());
        gpsTracker.callback = new GPSTracker.GPSTrackerCallback() {
            @Override
            public void locationChangedCallback(Location location) {
                updateCurrentWeather(location.getLatitude(), location.getLongitude());
            }
        };
        if (gpsTracker.getIsGPSTrackingEnabled())
        {
            double latitude = gpsTracker.getLatitude();
            double longitude = gpsTracker.getLongitude();
            updateCurrentWeather(latitude, longitude);
        }
        else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gpsTracker.showSettingsAlert();
        }
    }

    private void updateCurrentWeather(double latitude, double longitude){
        // Add a marker in Sydney and move the camera
        LatLng saigon = new LatLng(latitude, longitude);
        ggMap.addMarker(new MarkerOptions().position(saigon));
        ggMap.moveCamera(CameraUpdateFactory.newLatLng(saigon));

        //Get updates
        callRestToGetCurrentWeather(latitude, longitude);
    }

    private void callRestToGetCurrentWeather(double latitude, double longitude){
        if (isAdded()) {
            DialogUtil.getInstance().initDialog(getString(R.string.loading_title), getString(R.string.getting_weather_info_of) + " my location", getActivity());
            communicator.getCurrentWeatherOfCityByLatLong(latitude, longitude);
        }
    }

    @Subscribe
    public void onServerEvent(ServerEvent serverEvent){
        DialogUtil.getInstance().dismissDialog();
        final CurrentLocation currentLocation = serverEvent.getForecastResponse().getCurrentLocation();
        Current currentWeather = serverEvent.getForecastResponse().getCurrent();

        //Top view
        textViewCityName.setText(currentLocation.getName() + ", " + currentLocation.getCountry());
        textViewCondition.setText(currentWeather.getCondition().getText());
        textViewTemperature.setText(String.format("%.0f", currentWeather.getTempC()));
        LoadImageUtil.loadImage(currentWeather.getCondition().getIcon(), imageViewCondition, getActivity());
        buttonNext.setVisibility(View.VISIBLE);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Country country = new Country(currentLocation.getName());
                ((MapFragmentListener)getActivity()).onOpenCityDetail(country);
            }
        });

        ((MapFragmentListener)getActivity()).onEnableShareButtonEvent(currentLocation.getName(), currentWeather.getTempC());
    }

    @Subscribe
    public void onErrorEvent(ErrorEvent errorEvent){
        DialogUtil.getInstance().dismissDialog();
        Toast.makeText(getActivity(),""+errorEvent.getErrorMsg(), Toast.LENGTH_SHORT).show();
    }

    public interface MapFragmentListener {
        public void onEnableShareButtonEvent(String cityName, float tempC);
        public void onDisableShareButtonEvent();
        public void onOpenCityDetail(Country country);
    }
}



