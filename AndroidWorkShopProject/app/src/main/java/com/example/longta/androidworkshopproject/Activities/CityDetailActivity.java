package com.example.longta.androidworkshopproject.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ShareActionProvider;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.longta.androidworkshopproject.Adapter.ForecastHourAdapter;
import com.example.longta.androidworkshopproject.Adapter.ForecastNextDaysAdapter;
import com.example.longta.androidworkshopproject.JsonModels.Current;
import com.example.longta.androidworkshopproject.JsonModels.CurrentLocation;
import com.example.longta.androidworkshopproject.JsonModels.Forecastday;
import com.example.longta.androidworkshopproject.JsonModels.Hour;
import com.example.longta.androidworkshopproject.Models.Country;
import com.example.longta.androidworkshopproject.Models.ErrorEvent;
import com.example.longta.androidworkshopproject.Models.ServerEvent;
import com.example.longta.androidworkshopproject.R;
import com.example.longta.androidworkshopproject.RESTFactory.BusProvider;
import com.example.longta.androidworkshopproject.RESTFactory.Communicator;
import com.example.longta.androidworkshopproject.Util.DialogUtil;
import com.example.longta.androidworkshopproject.Util.LoadImageUtil;
import com.example.longta.androidworkshopproject.Util.SharingUtil;
import com.example.longta.androidworkshopproject.Util.StorageUtil;
import com.squareup.otto.Subscribe;

import java.util.List;

import static com.example.longta.androidworkshopproject.Activities.MainActivity.CURRENT_CITY;

/**
 * Created by longta on 5/31/17.
 */

public class CityDetailActivity extends AppCompatActivity{

    private Communicator communicator;
    Country currentCountry;

    private TextView textViewCityName, textViewCondition, textViewTemperature, textViewPrecipitation, textViewHumidity, textViewWind;
    private ImageView imageViewCondition;
    private Button buttonNext;

    private static final int MENU_SHARE = Menu.FIRST;
    private static final int MENU_ADD_BOOKMARK = Menu.FIRST + 1;
    private static final int MENU_REMOVE_BOOKMARK = Menu.FIRST + 2;
    private Current currentWeather;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_detail);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        currentCountry = (Country) intent.getSerializableExtra(CURRENT_CITY);

        textViewCityName = (TextView) findViewById(R.id.text_city_name);
        textViewCondition = (TextView) findViewById(R.id.text_condition);
        textViewTemperature = (TextView) findViewById(R.id.text_temperature);
        textViewPrecipitation = (TextView) findViewById(R.id.text_precipitation);
        textViewHumidity = (TextView) findViewById(R.id.text_humidity);
        textViewWind = (TextView) findViewById(R.id.text_wind);
        imageViewCondition = (ImageView) findViewById(R.id.imageView_condition);
        buttonNext = (Button)findViewById(R.id.button_next);

        communicator = new Communicator();
        getCurrentWeather(currentCountry.getCityName());

        //Your toolbar is now an action bar and you can use it like you always do, for example:
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(currentCountry.getCityName());

        //show dialog
        DialogUtil.getInstance().initDialog(getString(R.string.loading_title), getString(R.string.getting_weather_info_of) + " " + currentCountry.getCityName(), this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        //if city still not bookmarked
        menu.clear();

        menu.add(0, MENU_SHARE, Menu.NONE, R.string.action_share);
        MenuItem itemShare = menu.findItem(MENU_SHARE);
        ShareActionProvider shareActionProvider = new ShareActionProvider(this);
        MenuItemCompat.setActionProvider(itemShare, shareActionProvider);

        if (StorageUtil.isCountryBookmarked(currentCountry, this)){
            menu.add(0, MENU_REMOVE_BOOKMARK, Menu.NONE, R.string.action_remove_bookmark);
        }
        else {
            menu.add(0, MENU_ADD_BOOKMARK, Menu.NONE, R.string.action_add_bookmark);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        int id = menuItem.getItemId();
        switch (id){
            case MENU_SHARE:
                //do stuff share
                shareWeather();
                return true;
            case MENU_ADD_BOOKMARK:
                //do stuff add bookmark
                addRemoveCountryFromBookmark();
                return true;
            case MENU_REMOVE_BOOKMARK:
                //do stuff remove bookmark
                addRemoveCountryFromBookmark();
                return true;
            default:
                onBackPressed();
                return true;
        }
    }

    private void shareWeather(){
        if (currentWeather != null){
            SharingUtil.shareWeatherInfo(currentCountry.getCityName(), currentWeather.getTempC(), this);
        }
        else {
            Toast.makeText(this, "Unable to get current weather of " + currentCountry.getCityName(), Toast.LENGTH_LONG).show();
        }
    }

    private void addRemoveCountryFromBookmark(){
        StorageUtil.bookmarkCountry(currentCountry, this);
        supportInvalidateOptionsMenu();
    }
    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        return super.onCreateView(name, context, attrs);
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Subscribe
    public void onServerEvent(ServerEvent serverEvent){
        DialogUtil.getInstance().dismissDialog();
        CurrentLocation currentLocation = serverEvent.getForecastResponse().getCurrentLocation();
        currentWeather = serverEvent.getForecastResponse().getCurrent();

        //Top view
        textViewCityName.setText(currentCountry.getCityName() + ", " + currentLocation.getCountry());
        textViewCondition.setText(currentWeather.getCondition().getText());
        textViewTemperature.setText(String.format("%.0f", currentWeather.getTempC()));
        textViewPrecipitation.setText(textViewPrecipitation.getText() + String.format("%.0f", currentWeather.getPrecipMm()*100)+"%");
        textViewHumidity.setText(textViewHumidity.getText() + String.format("%.0f", currentWeather.getHumidity())+"%");
        textViewWind.setText(textViewWind.getText() + String.format("%.0f", currentWeather.getWindKph())+" km/h");
        LoadImageUtil.loadImage(currentWeather.getCondition().getIcon(), imageViewCondition, this);

        //create forecast next hours
        List<Hour>hourList = serverEvent.getForecastResponse().getForecast().getForecastday().get(0).getHour();
        createForecastHourViewholder(hourList);

        //create forecast next days
        List<Forecastday>forecastdayList = serverEvent.getForecastResponse().getForecast().getForecastday();
        createForecastNextDayViewholder(forecastdayList);
    }

    @Subscribe
    public void onErrorEvent(ErrorEvent errorEvent){
        DialogUtil.getInstance().dismissDialog();
        Toast.makeText(this,""+errorEvent.getErrorMsg(), Toast.LENGTH_SHORT).show();
    }

    private void getCurrentWeather(String cityCode){
        communicator.getCurrentWeatherOfCity(cityCode);
    }

    private void createForecastHourViewholder(List<Hour>hourList){
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyler_view_forecast_next_hours);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayout.HORIZONTAL);
        recyclerView.setLayoutManager(manager);
        ForecastHourAdapter forecastHourAdapter = new ForecastHourAdapter(this, hourList);
        recyclerView.setAdapter(forecastHourAdapter);
    }

    private void createForecastNextDayViewholder(List<Forecastday>forecastdayList){
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyler_view_forecast_next_days);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayout.HORIZONTAL);
        recyclerView.setLayoutManager(manager);
        ForecastNextDaysAdapter forecastNextDaysAdapter = new ForecastNextDaysAdapter(this, forecastdayList);
        recyclerView.setAdapter(forecastNextDaysAdapter);
    }
}
