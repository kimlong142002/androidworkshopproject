package com.example.longta.androidworkshopproject.Util;

import android.content.Context;
import android.content.Intent;

/**
 * Created by longta on 6/23/17.
 */

public class SharingUtil {

    public static void shareWeatherInfo(String cityName, float tempC, Context context){
        String shareText = "Hi, current temperature of " + cityName + " is: " + String.format("%.0f", tempC) + "\u2103";
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareText);
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
    }
}
