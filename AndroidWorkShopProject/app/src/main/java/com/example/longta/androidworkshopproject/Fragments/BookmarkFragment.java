package com.example.longta.androidworkshopproject.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.longta.androidworkshopproject.Models.Country;
import com.example.longta.androidworkshopproject.R;
import com.example.longta.androidworkshopproject.Util.StorageUtil;

import java.util.ArrayList;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

/**
 * Created by longta on 5/28/17.
 */

public class BookmarkFragment extends HomeFragment {

    public static BookmarkFragment newInstance(){
        return new BookmarkFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        loadCountries(context);
    }

    @Override
    public void loadCountries(Context context) {
        ArrayList<String> listBookmarkCountries = StorageUtil.getListBookmarkedCountries(context);
        mCountries.clear();
        for (int i = 0; i < listBookmarkCountries.size(); i++){
            Country country = new Country(listBookmarkCountries.get(i));
            mCountries.add(country);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_home, container, false);
        //Init Recycler view
        initRecyclerViewFromRootView(view);
        SearchView searchView = (SearchView)view.findViewById(R.id.search_view_home);
        searchView.setVisibility(View.GONE);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadCountries(getActivity());
        Log.e(TAG, "list bookmarked city" + mCountries);
        cityAdapter.notifyDataSetChanged();
    }
}
