package com.example.longta.androidworkshopproject.RESTFactory;

import android.util.Log;

import com.example.longta.androidworkshopproject.JsonModels.ForecastResponse;
import com.example.longta.androidworkshopproject.JsonModels.SearchResult;
import com.example.longta.androidworkshopproject.Models.ErrorEvent;
import com.example.longta.androidworkshopproject.Models.SearchEvent;
import com.example.longta.androidworkshopproject.Models.ServerEvent;
import com.squareup.otto.Produce;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by longta on 5/29/17.
 */

public class Communicator {
    private static  final String TAG = "Communicator";
    private static final String SERVER_URL = "http://api.apixu.com/";
    private static final String API_KEY = "7029fa484c7249b79aa144310172805";
    private static final String NO_DAYS = "5";

    public Retrofit mMetrofit(){
        //Here a logging interceptor is created
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        //The logging interceptor will be added to the http client
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        //The Retrofit builder will have the client attached, in order to get connection logs
        Retrofit retrofit = new Retrofit.Builder()
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(SERVER_URL)
                .build();

        return retrofit;
    }

    public void getCurrentWeatherOfCity(String cityCode){
        //The Retrofit builder will have the client attached, in order to get connection logs
        Retrofit retrofit = mMetrofit();

        ForecastInterface service = retrofit.create(ForecastInterface.class);
        Call<ForecastResponse> call = service.get(API_KEY, cityCode, NO_DAYS);

        Log.d(TAG, "getCurrentWeatherOfCity: " + call.request().url());
        call.enqueue(new Callback<ForecastResponse>() {
            @Override
            public void onResponse(Call<ForecastResponse> call, Response<ForecastResponse> response) {
                BusProvider.getInstance().post(new ServerEvent(response.body()));
                Log.e(TAG, "Success");
            }

            @Override
            public void onFailure(Call<ForecastResponse> call, Throwable t) {
                // handle execution failures like no internet connectivity
                BusProvider.getInstance().post(new ErrorEvent(-2, t.getMessage()));
            }
        });
    }

    public void getCurrentWeatherOfCityByLatLong(double lattitude, double longitude){
        String parameter = Double.toString(lattitude) + "," + Double.toString(longitude);
        getCurrentWeatherOfCity(parameter);
    }

    //Search location
    public void searchLocation(String queryString){
        //The Retrofit builder will have the client attached, in order to get connection logs
        Retrofit retrofit = mMetrofit();

        SearchInterface service = retrofit.create(SearchInterface.class);
        Call<List<SearchResult>> call = service.get(API_KEY, queryString);

        Log.d(TAG, "searchLocation: " + call.request().url());
        call.enqueue(new Callback<List<SearchResult>>() {
            @Override
            public void onResponse(Call<List<SearchResult>> call, Response<List<SearchResult>> response) {
                BusProvider.getInstance().post(new SearchEvent(response.body()));
                Log.e(TAG, "Success");
            }

            @Override
            public void onFailure(Call<List<SearchResult>> call, Throwable t) {
                // handle execution failures like no internet connectivity
                BusProvider.getInstance().post(new ErrorEvent(-2, t.getMessage()));
            }
        });
    }

    @Produce
    public ServerEvent produceServerEvent(ForecastResponse forecastResponse) {
        return new ServerEvent(forecastResponse);
    }

    public SearchEvent produceSearchEvent(List<SearchResult> searchResults){
        return new SearchEvent(searchResults);
    }

    @Produce
    public ErrorEvent produceErrorEvent(int errorCode, String errorMsg) {
        return new ErrorEvent(errorCode, errorMsg);
    }
}
