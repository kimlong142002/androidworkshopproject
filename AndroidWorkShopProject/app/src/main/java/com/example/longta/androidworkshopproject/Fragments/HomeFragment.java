package com.example.longta.androidworkshopproject.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.longta.androidworkshopproject.JsonModels.SearchResult;
import com.example.longta.androidworkshopproject.Models.Country;
import com.example.longta.androidworkshopproject.Models.ErrorEvent;
import com.example.longta.androidworkshopproject.Models.SearchEvent;
import com.example.longta.androidworkshopproject.R;
import com.example.longta.androidworkshopproject.RESTFactory.BusProvider;
import com.example.longta.androidworkshopproject.RESTFactory.Communicator;
import com.example.longta.androidworkshopproject.Util.DialogUtil;
import com.example.longta.androidworkshopproject.Util.StorageUtil;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by longta on 5/28/17.
 */

public class HomeFragment extends Fragment{
    private static  final String TAG = "HomeFragment";

    public ArrayList<Country> mCountries = new ArrayList<Country>();
    public List<SearchResult> mSearchResults;
    public CityAdapter cityAdapter;
    private OnCitySelected mListener;
    private Communicator communicator;

    public static HomeFragment newInstance(){
        return new HomeFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        communicator = new Communicator();
        BusProvider.getInstance().register(this);

        if (context instanceof OnCitySelected){
            mListener = (OnCitySelected)context;
        }else {
            throw new ClassCastException(context.toString() + " must implement OnRageComicSelected.");
        }

        //load default cities
        Resources resources = context.getResources();
        String[] cities = resources.getStringArray(R.array.cities);
        for (int i = 0; i < cities.length; i++){
            Country country = new Country(cities[i]);
            mCountries.add(country);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_home, container, false);

        //Init Recycler view
        initRecyclerViewFromRootView(view);

        //Init Search view
        initSearchViewFromRootView(view);

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        BusProvider.getInstance().unregister(this);
    }

    //Other methods
    public void loadCountries(Context context){
        mCountries.clear();
        for (int i = 0; i < mSearchResults.size(); i++){
            SearchResult searchResult = mSearchResults.get(i);
            Country country = new Country(searchResult.getName());
            mCountries.add(country);
        }
    }

    public void initRecyclerViewFromRootView(View view){
        final Activity activity = getActivity();
        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.home_cities_recycle_view);
        LinearLayoutManager manager = new LinearLayoutManager(activity);
        manager.setOrientation(LinearLayout.VERTICAL);
        recyclerView.setLayoutManager(manager);
        cityAdapter = new CityAdapter(activity);
        recyclerView.setAdapter(cityAdapter);
    }

    private void initSearchViewFromRootView(View view){
        final SearchView searchView = (SearchView)view.findViewById(R.id.search_view_home);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener(){
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchView.clearFocus();
                callRestForSearchingCity(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });
    }

    private void callRestForSearchingCity(String queryString){
        Log.d(TAG, "Search city: " + queryString);
        DialogUtil.getInstance().initDialog(getString(R.string.loading_title), getString(R.string.looking_for_your_wish), getActivity());
        communicator.searchLocation(queryString);
    }

    //Service Response
    @Subscribe
    public void onSearchEvent(SearchEvent searchEvent){
        DialogUtil.getInstance().dismissDialog();
        mSearchResults = searchEvent.getSearchResults();
        loadCountries(getActivity());
        cityAdapter.notifyDataSetChanged();
    }

    @Subscribe
    public void onErrorEvent(ErrorEvent errorEvent){
        DialogUtil.getInstance().dismissDialog();
        Toast.makeText(this.getActivity(),""+errorEvent.getErrorMsg(), Toast.LENGTH_SHORT).show();
    }


    class CityAdapter extends RecyclerView.Adapter<ViewHolder> implements ViewHolder.CityCellListener{

        private LayoutInflater mLayoutInflater;
        private Context mContext;

    public CityAdapter(Context context) {
            mLayoutInflater = LayoutInflater.from(context);
            mContext = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            return new ViewHolder(mLayoutInflater
                    .inflate(R.layout.recycler_item_city, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, final int position) {
            final Country country = mCountries.get(position);
            viewHolder.setData(country, this, mContext);

            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onCitySelected(country);
                }
            });
        }

        @Override
        public int getItemCount() {
            return mCountries.size();
        }

        @Override
        public void onBookmarkUpdate(Country country) {
            mListener.onCityAddOrRemoveBookmark(country);
            loadCountries(getActivity());
            cityAdapter.notifyDataSetChanged();
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        // Views
        private TextView mNameTextView;
        private Button mButtonBookmark;

        private ViewHolder(View itemView) {
            super(itemView);
            mNameTextView = (TextView) itemView.findViewById(R.id.city_name);
            mButtonBookmark = (Button)itemView.findViewById(R.id.button_bookmark);
        }

        private void setData(final Country country, final CityAdapter cityAdapter, Context context) {
            mNameTextView.setText(country.getCityName());
            if (StorageUtil.isCountryBookmarked(country, context)){
                mButtonBookmark.setBackgroundResource(R.drawable.ic_bookmarked);
            }
            else {
                mButtonBookmark.setBackgroundResource(R.drawable.ic_bookmark);
            }
            mButtonBookmark.setOnClickListener(new Button.OnClickListener(){
                @Override
                public void onClick(View v) {
                    ((CityCellListener)cityAdapter).onBookmarkUpdate(country);
                }
            });
        }

        interface CityCellListener{
           public void onBookmarkUpdate(Country country);
        }
    }

    public interface OnCitySelected {
        void onCitySelected(Country country);
        void onCityAddOrRemoveBookmark(Country country);
    }
}
