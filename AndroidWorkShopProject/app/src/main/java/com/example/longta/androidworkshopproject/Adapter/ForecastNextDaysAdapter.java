package com.example.longta.androidworkshopproject.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.longta.androidworkshopproject.JsonModels.Forecastday;
import com.example.longta.androidworkshopproject.R;
import com.example.longta.androidworkshopproject.ViewHolder.ForecastNextDaysViewHolder;

import java.text.ParseException;
import java.util.List;

/**
 * Created by longta on 6/6/17.
 */

public class ForecastNextDaysAdapter extends RecyclerView.Adapter {
    private LayoutInflater mLayoutInflater;
    public List<Forecastday> mForecastdayList;
    public Context mContext;

    public ForecastNextDaysAdapter(Context context, List<Forecastday>forecastdayList){
        mContext = context;
        mLayoutInflater = LayoutInflater.from(mContext);
        mForecastdayList = forecastdayList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ForecastNextDaysViewHolder(mLayoutInflater.inflate(R.layout.recycler_view_forecast_day, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ForecastNextDaysViewHolder forecastdayViewHolder = (ForecastNextDaysViewHolder)holder;
        Forecastday forecastday = mForecastdayList.get(position);
        try {
            forecastdayViewHolder.setData(forecastday, mContext);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mForecastdayList.size();
    }
}
