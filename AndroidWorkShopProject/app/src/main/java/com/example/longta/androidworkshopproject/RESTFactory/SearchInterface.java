package com.example.longta.androidworkshopproject.RESTFactory;

import com.example.longta.androidworkshopproject.JsonModels.SearchResult;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by longta on 6/24/17.
 */

public interface SearchInterface {
    //This method is used for "GET"
    @GET("v1/search.json?")
    Call<List<SearchResult>> get(
            @Query("key") String key,
            @Query("q") String query
    );
}
