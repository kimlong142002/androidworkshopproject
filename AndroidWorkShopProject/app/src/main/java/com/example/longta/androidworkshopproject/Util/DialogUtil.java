package com.example.longta.androidworkshopproject.Util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by longta on 6/24/17.
 */

public class DialogUtil {
    private static DialogUtil _dialogUtil = null;
    public ProgressDialog progressDialog = null;
    private Context mContext = null;

    public static DialogUtil getInstance()
    {
        if (_dialogUtil == null)
            _dialogUtil = new DialogUtil();

        return _dialogUtil;
    }

    public void initDialog(String title, String message, Context context){
        //show dialog
        if (progressDialog == null || !(mContext.getClass().equals(context.getClass())) || mContext != context){
            if (progressDialog != null){
                progressDialog.dismiss();
            }
            progressDialog = new ProgressDialog(context);
            mContext = context;
        }
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        if (!((Activity)context).isFinishing()){
            progressDialog.show();
        }
    }

    public void dismissDialog(){
        progressDialog.dismiss();
    }
}
