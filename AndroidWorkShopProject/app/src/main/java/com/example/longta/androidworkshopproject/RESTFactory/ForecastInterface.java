package com.example.longta.androidworkshopproject.RESTFactory;

import com.example.longta.androidworkshopproject.JsonModels.ForecastResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by longta on 5/29/17.
 */

public interface ForecastInterface {
    //This method is used for "GET"
    @GET("v1/forecast.json?")
    Call<ForecastResponse> get(
            @Query("key") String key,
            @Query("q") String query,
            @Query("days") String numberOfDays
    );
}
