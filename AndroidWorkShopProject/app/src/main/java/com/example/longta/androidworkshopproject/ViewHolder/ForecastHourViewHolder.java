package com.example.longta.androidworkshopproject.ViewHolder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.longta.androidworkshopproject.JsonModels.Hour;
import com.example.longta.androidworkshopproject.R;
import com.example.longta.androidworkshopproject.Util.LoadImageUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * Created by longta on 6/6/17.
 */

public class ForecastHourViewHolder extends RecyclerView.ViewHolder {
    // Views
    private TextView mTextHour, mTextTemp;
    private ImageView mImageViewCondition;

    public ForecastHourViewHolder(View itemView) {
        super(itemView);

        mTextHour = (TextView) itemView.findViewById(R.id.text_hour);
        mTextTemp = (TextView) itemView.findViewById(R.id.text_temp);
        mImageViewCondition = (ImageView) itemView.findViewById(R.id.imageView_hour_condition);
    }

    public void setData(Hour hour, Context context) throws ParseException {
        SimpleDateFormat dateFormatOrigin = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = dateFormatOrigin.parse(hour.getTime());
        SimpleDateFormat dateFormatNew = new SimpleDateFormat("h a");
        String formatedHour = dateFormatNew.format(date);
        mTextHour.setText(formatedHour);

        String temperatureString = String.format("%.0f", hour.getTempC());
        mTextTemp.setText(temperatureString);

        LoadImageUtil.loadImage(hour.getCondition().getIcon(), mImageViewCondition, context);
    }
}
