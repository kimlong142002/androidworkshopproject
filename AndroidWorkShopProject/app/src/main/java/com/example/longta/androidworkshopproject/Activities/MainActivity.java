package com.example.longta.androidworkshopproject.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.example.longta.androidworkshopproject.Fragments.BookmarkFragment;
import com.example.longta.androidworkshopproject.Fragments.HomeFragment;
import com.example.longta.androidworkshopproject.Fragments.MapFragment;
import com.example.longta.androidworkshopproject.Models.Country;
import com.example.longta.androidworkshopproject.R;
import com.example.longta.androidworkshopproject.Util.SharingUtil;
import com.example.longta.androidworkshopproject.Util.StorageUtil;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener , HomeFragment.OnCitySelected, MapFragment.MapFragmentListener{

    private static final int MENU_SHARE = Menu.FIRST;

    public static final String CURRENT_CITY = "CurrentCity";
    private Bundle mSavedInstanceState;
    private NavigationView mMavigationView;

    private String mapStringCityName;
    private float mapTempC;
    private boolean shouldShowShareButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSavedInstanceState = savedInstanceState;

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        mMavigationView = (NavigationView) findViewById(R.id.nav_view);
        mMavigationView.setNavigationItemSelectedListener(this);

        //add home as default
        MenuItem homeMenu = mMavigationView.getMenu().getItem(0);
        homeMenu.setChecked(true);
        onNavigationItemSelected(homeMenu);

        //request location access
        requestLocationAccess();
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void requestLocationAccess(){
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
    }
    private void addHomeFragment(){
        final HomeFragment homeFragment = HomeFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root_layout, homeFragment, "Home")
                .commit();
        getSupportActionBar().setTitle(R.string.toolbar_title_home);
    }

    private void addMapFragment(){
        final MapFragment mapFragment = MapFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root_layout, mapFragment, "Map")
                .commit();
        getSupportActionBar().setTitle(R.string.toolbar_title_map);
    }


    private void addBookmarkFragment(){
        final BookmarkFragment bookmarkFragment = BookmarkFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root_layout, bookmarkFragment, "Bookmark")
                .commit();
        getSupportActionBar().setTitle(R.string.toolbar_title_bookmark);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            resetTitle(mMavigationView);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            addHomeFragment();
        } else if (id == R.id.nav_map) {
            addMapFragment();
        } else if (id == R.id.nav_bookmark) {
            addBookmarkFragment();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //MapFragmentListener
    @Override
    public void onCitySelected(Country country) {
        openCitiDetailActivity(country);
    }

    @Override
    public void onCityAddOrRemoveBookmark(Country country) {
        StorageUtil.bookmarkCountry(country, this);
    }

    @Override
    public void onOpenCityDetail(Country country) {
        openCitiDetailActivity(country);
    }

    public void openCitiDetailActivity(Country country){
        Intent intent = new Intent(this, CityDetailActivity.class);
        intent.putExtra(CURRENT_CITY, country);
        startActivity(intent);
    }

    private void resetTitle(NavigationView navigationView) {
        Menu menu = navigationView.getMenu();
        for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            if (item.isChecked()) {
                getSupportActionBar().setTitle(item.getTitle());
                break;
            }
        }
    }

    //MapFragmentListener
    @Override
    public void onEnableShareButtonEvent(String cityName, float tempC) {
        shouldShowShareButton = true;
        mapStringCityName = cityName;
        mapTempC = tempC;
        supportInvalidateOptionsMenu();
    }

    @Override
    public void onDisableShareButtonEvent() {
        shouldShowShareButton = false;
        supportInvalidateOptionsMenu();
    }

    //Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        //if city still not bookmarked
        menu.clear();

        if (shouldShowShareButton){
            menu.add(0, MENU_SHARE, Menu.NONE, R.string.action_share);
            MenuItem itemShare = menu.findItem(MENU_SHARE);
            ShareActionProvider shareActionProvider = new ShareActionProvider(this);
            MenuItemCompat.setActionProvider(itemShare, shareActionProvider);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        int id = menuItem.getItemId();
        switch (id){
            case MENU_SHARE:
                //do stuff share
                shareWeather();
                return true;
            default:
                onBackPressed();
                return true;
        }
    }

    private void shareWeather(){
        if (mapStringCityName.length() > 0){
            SharingUtil.shareWeatherInfo(mapStringCityName, mapTempC, this);
        }
    }
}