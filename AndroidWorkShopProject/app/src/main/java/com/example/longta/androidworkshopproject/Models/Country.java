package com.example.longta.androidworkshopproject.Models;

import java.io.Serializable;

/**
 * Created by longta on 6/9/17.
 */

public class Country implements Serializable {

    private String cityName;
    public Country(String cityName){
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
